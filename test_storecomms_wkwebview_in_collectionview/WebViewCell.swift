//
//  WebViewCell.swift
//  test_storecomms_wkwebview_in_collectionview
//
//  Created by Brigitte Michau on 2018/10/01.
//  Copyright © 2018 Brigitte Michau. All rights reserved.
//

import UIKit
import WebKit

class WebViewCell: UICollectionViewCell {
    
    let webView: WKWebView = {
        let view = WKWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var html: String? {
        didSet {
            loadHTML()
        }
    }
    
    override func awakeFromNib() {
        
        contentView.backgroundColor = UIColor.red
        
        contentView.addSubview(webView)
        
        let constraints: [NSLayoutConstraint] = [
            webView.topAnchor.constraint(equalTo: contentView.topAnchor),
            webView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            webView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    func loadHTML() {
        if let html = self.html {
            webView.loadHTMLString(html, baseURL: nil)
        }
    }
}

