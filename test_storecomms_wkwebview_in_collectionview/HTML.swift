//
//  HTML.swift
//  test_storecomms_wkwebview_in_collectionview
//
//  Created by Brigitte Michau on 2018/10/01.
//  Copyright © 2018 Brigitte Michau. All rights reserved.
//

import Foundation

struct HTML: Codable {
    let body: String
}
