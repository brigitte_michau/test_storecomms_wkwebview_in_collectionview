//
//  test_storecomms_wkwebview_in_collectionviewUITests.swift
//  test_storecomms_wkwebview_in_collectionviewUITests
//
//  Created by Brigitte Michau on 2018/10/01.
//  Copyright © 2018 Brigitte Michau. All rights reserved.
//

import XCTest

class test_storecomms_wkwebview_in_collectionviewUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
