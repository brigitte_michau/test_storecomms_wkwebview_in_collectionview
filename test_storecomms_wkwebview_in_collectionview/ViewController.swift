//
//  ViewController.swift
//  test_storecomms_wkwebview_in_collectionview
//
//  Created by Brigitte Michau on 2018/10/01.
//  Copyright © 2018 Brigitte Michau. All rights reserved.
//
import UIKit
import WebKit

private let reuseIdentifier = "WebViewCell"

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!

    var webViewSizes = [(WKWebView, CGSize)]()
    
    var items = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = UIColor.gray
        
        items = loadJSONFile()
        
        for item in items {
            let webView = WKWebView()
            let webViewSize = (webView, CGSize.zero)
            webViewSizes.append(webViewSize)
            webView.navigationDelegate = self
            webView.loadHTMLString(item, baseURL: nil)
        }
    }

    func loadJSONFile() -> [String] {
        guard let filePath = Bundle.main.path(forResource: "storecommhtml", ofType: "json", inDirectory: nil) else {
            fatalError("No such file")
        }
        do {
            let fileUrl = URL(fileURLWithPath: filePath)
            let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let decoder = JSONDecoder()
            let bodies = try decoder.decode([HTML].self, from: data)
            
            dump(bodies)
            
            var strings = [String]()
            
            for item in bodies {
                strings.append(item.body)
            }
            
            return(strings)
            
        } catch {
            print(error)
            fatalError("Unable to read contents of the file url")
        }
        fatalError("No data")
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? WebViewCell else { return UICollectionViewCell() }
        cell.html = items[indexPath.row]
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let webViewSize = webViewSizes[indexPath.row]
        return webViewSize.1
        
    }
}

extension ViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        webView.evaluateJavaScript("document.height") { [weak self, collectionView] (height, error) in
        //webView.evaluateJavaScript("document.body.offsetHeight") { [weak self, collectionView] (height, error) in
            guard let height = height as? CGFloat else { return }

            
            let index = self?.webViewSizes.firstIndex(where: { (wv, _) -> Bool in
                wv == webView
            })
            
            if let index = index {
                self?.webViewSizes[index] = (webView, CGSize(width: collectionView?.bounds.width ?? 0, height: height))
               
                print("=============================")
                print(index)
                print(height)
            }
           
            if let numberOfItems = collectionView?.numberOfItems(inSection: 0) {
                if index == numberOfItems - 1 {
                    collectionView?.reloadData()
                }
            }
        }
    }
}
